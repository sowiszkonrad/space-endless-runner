﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScreenBounds
{
    public static Vector2 GetScreenBounds(Camera camera)
    {
        return camera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, camera.transform.position.z));
    }
}
