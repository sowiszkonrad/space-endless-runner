﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScrollSpeed : MonoBehaviour
{
    private float scrollSpeed;

    public float scrollVelocity;
    public float maxScrollSpeed;


    private void Update()
    {
        transform.Translate(Vector2.up * Time.deltaTime * scrollSpeed);
        if (scrollSpeed <= maxScrollSpeed)
        {
            scrollSpeed += scrollVelocity * Time.deltaTime;
        }
    }
}
