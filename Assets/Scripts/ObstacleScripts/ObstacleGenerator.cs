﻿using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour
{
    [SerializeField] private List<GameObject> obstacelsToGenerate;
    [SerializeField] private float distanceBetweenObstacles;
    private GameObject obstaclesParent;
    private float startingPositionY;
    private Vector2 screenBounds = ScreenBounds.GetScreenBounds(Camera.main);

    private void Start()
    {
        startingPositionY = screenBounds.y;
        OnScreenExit.obstacleLeftScreen += CreateNewObstacle;
        CreateFirstObstacles();
    }

    private void CreateFirstObstacles()
    {
        for (int i = 0; i < 3; i++)
        {
            CreateNewObstacle();
        }
    }

    private void CreateNewObstacle()
    {
        GameObject obstacle = ObjectPooler.instance.GetPooledObject(obstacelsToGenerate[Random.Range(0, obstacelsToGenerate.Count)].tag);
        if (obstacle != null)
        {
            obstacle.SetActive(true);

            float randomXPosition = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0, Camera.main.pixelWidth), 0, 0)).x;
            randomXPosition = Mathf.Clamp(randomXPosition, screenBounds.x * -1 + SizeXOfGameObject(obstacle), screenBounds.x - SizeXOfGameObject(obstacle));
            obstacle.transform.position = new Vector2(randomXPosition, startingPositionY);
            obstacle.transform.rotation = Quaternion.identity;
            if (obstacle.transform.childCount > 0)
            {
                foreach (Transform child in obstacle.transform)
                {
                    child.gameObject.SetActive(true);
                }
            }
            startingPositionY += distanceBetweenObstacles;
        }
    }

    private float SizeXOfGameObject(GameObject obj)
    {
        SpriteRenderer spriteRenderer;
        if (obj.TryGetComponent<SpriteRenderer>(out spriteRenderer))
        {
            return spriteRenderer.bounds.extents.x;
        }
        else
        {
            return obj.GetComponent<BoxCollider2D>().bounds.extents.x * 2f;
        }
    }
}