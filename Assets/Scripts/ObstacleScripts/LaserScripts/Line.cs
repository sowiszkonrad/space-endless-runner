﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer), typeof(BoxCollider2D))]
public class Line : MonoBehaviour
{
    private GameObject gameObject1;
    private GameObject gameObject2;
    private BoxCollider2D boxCollider;

    [SerializeField] private float lineWidth;
    [SerializeField] private Gradient lineColor;

    private LineRenderer line;

    private void Awake()
    {
        line = GetComponent<LineRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();
    }

    public void CreateLineRenderBeetweenChilds()
    {
        line.startWidth = lineWidth;
        line.endWidth = lineWidth;
        line.colorGradient = lineColor;
        line.positionCount = 2;

        gameObject2 = transform.GetChild(1).gameObject;
        gameObject1 = transform.GetChild(0).gameObject;

        if (gameObject1 != null && gameObject2 != null)
        {
            line.SetPosition(0, gameObject1.transform.position);
            line.SetPosition(1, gameObject2.transform.position);
            float lineLength = Vector2.Distance(gameObject1.transform.position, gameObject2.transform.position);
            boxCollider.size = new Vector2(lineLength, lineWidth);
            //Vector3 midPoint = (gameObject1.transform.position + gameObject2.transform.position) / 2;
            //boxCollider.offset = new Vector2(midPoint.x, 0);
        }
    }

    private void Update()
    {
        if (line.positionCount == 2)
        {
            line.SetPosition(0, gameObject1.transform.position);
            line.SetPosition(1, gameObject2.transform.position);
        }
    }
}