﻿using System.Collections;
using UnityEngine;

public class LaserObstacle : MonoBehaviour, IObstacle
{
    [SerializeField] private float minLaserLength;
    [SerializeField] private float maxLaserLength;

    public Line line;

    private void Awake()
    {
        line = GetComponent<Line>();
    }

    private void OnEnable()
    {
        OnCreate();
    }

    public void OnCreate()
    {
        float randomLength = Random.Range(minLaserLength, maxLaserLength);
        gameObject.transform.GetChild(0).transform.localPosition = new Vector2(-randomLength / 2, 0);
        gameObject.transform.GetChild(1).transform.localPosition = new Vector2(randomLength / 2, 0);
        CheckIfShouldRotate();
        line.CreateLineRenderBeetweenChilds();
    }

    private void CheckIfShouldRotate()
    {
        if (Random.Range(0, 2) == 1)
        {
            StartCoroutine(rotateObject(Random.Range(0, 10)));
        }
    }

    private IEnumerator rotateObject(float speed)
    {
        while (gameObject.activeInHierarchy)
        {
            transform.Rotate(new Vector3(0, 0, 200 * Time.deltaTime));
            yield return new WaitForEndOfFrame();
        }
    }
}