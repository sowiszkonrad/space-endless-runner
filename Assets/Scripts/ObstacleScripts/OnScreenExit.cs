﻿using System;
using UnityEngine;

public class OnScreenExit : MonoBehaviour
{
    public static event Action obstacleLeftScreen;

    [SerializeField] private bool isObstacle;

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "MainCamera")
        {
            gameObject.SetActive(false);
            if (isObstacle)
            {
                obstacleLeftScreen?.Invoke();
            }
        }
    }
}