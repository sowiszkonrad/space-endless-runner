﻿using System;
using UnityEngine;

public class CoinPickUp : MonoBehaviour
{
    public static event Action coinCollected;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            gameObject.SetActive(false);
            coinCollected?.Invoke();
        }
    }
}