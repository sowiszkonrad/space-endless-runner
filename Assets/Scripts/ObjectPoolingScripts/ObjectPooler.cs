﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler instance;

    private GameObject obstacleHolder;
    private List<GameObject> pooledObjects;

    [SerializeField] private List<ObjectPoolItem> itemToPool;

    private void Awake()
    {
        instance = this;
        InitializeObjects();
    }

    private void InitializeObjects()
    {
        obstacleHolder = new GameObject("ObstacleObject");
        pooledObjects = new List<GameObject>();
        foreach (ObjectPoolItem item in itemToPool)
        {
            for (int i = 0; i < item.amountToPool; i++)
            {
                InstantiatePooledObject(item);
            }
        }
    }

    public GameObject GetPooledObject(string tag)
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].CompareTag(tag))
            {
                return pooledObjects[i];
            }
        }
        foreach (ObjectPoolItem item in itemToPool)
        {
            if (item.objectToPool.name == name && item.shouldExpand)
            {
                return InstantiatePooledObject(item);
            }
        }
        return null;
    }

    private GameObject InstantiatePooledObject(ObjectPoolItem item)
    {
        GameObject obj = Instantiate(item.objectToPool, obstacleHolder.transform);
        obj.SetActive(false);
        pooledObjects.Add(obj);
        return obj;
    }
}