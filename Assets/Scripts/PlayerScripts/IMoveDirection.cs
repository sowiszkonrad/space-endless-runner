﻿public interface IMoveDirection
{
    float GetHorizontalDirection();

    float GetVerticalDirection();
}