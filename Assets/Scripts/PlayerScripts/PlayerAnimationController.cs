﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class PlayerAnimationController : MonoBehaviour
{
    private Animator animator;
    private IMoveDirection moveDirection;

    private void Start()
    {
        animator = GetComponent<Animator>();
        moveDirection = GetComponent<IMoveDirection>();
    }

    private void FixedUpdate()
    {
        animator.SetInteger("Direction", (int)moveDirection.GetHorizontalDirection());
    }
}
