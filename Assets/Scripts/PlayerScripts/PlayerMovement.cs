﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float playerSpeed;
    private IMoveDirection moveDirection;
    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        moveDirection = GetComponent<IMoveDirection>();
    }

    private void FixedUpdate()
    {
        if(moveDirection != null)
        {
            rb.velocity = new Vector2(moveDirection.GetHorizontalDirection() * Time.fixedDeltaTime * playerSpeed, 0);
        }
    }
}