﻿using UnityEngine;

public class MobileMoveDirection : MonoBehaviour, IMoveDirection
{
    public float GetHorizontalDirection()
    {
        return determineDirection(Screen.width);
    }

    public float GetVerticalDirection()
    {
        return determineDirection(Screen.height);
    }

    private int determineDirection(float screenSize)
    {
        int direction = 0;
        if (Input.touchCount > 0 && Input.GetTouch(0).position.x > screenSize / 2)
        {
            direction = 1;
        }
        else if (Input.touchCount > 0 && Input.GetTouch(0).position.x < screenSize / 2)
        {
            direction = -1;
        }
        else if (Input.touchCount == 0)
        {
            direction = 0;
        }
        return direction;
    }
}