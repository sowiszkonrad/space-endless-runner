﻿using UnityEngine;

public class ComputerMoveDirection : MonoBehaviour, IMoveDirection
{
    public float GetHorizontalDirection()
    {
        return Input.GetAxis("Horizontal");
    }

    public float GetVerticalDirection()
    {
        return Input.GetAxis("Vertical");
    }
}