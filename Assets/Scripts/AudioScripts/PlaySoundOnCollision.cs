﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlaySoundOnCollision : MonoBehaviour
{
    private AudioSource audioSource;
    [SerializeField] private AudioClip sound;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.playOnAwake = false;
        audioSource.clip = sound;
        CoinPickUp.coinCollected += PlayCoinSound;
    }

    private void PlayCoinSound()
    {
        audioSource.Play();
    }
}