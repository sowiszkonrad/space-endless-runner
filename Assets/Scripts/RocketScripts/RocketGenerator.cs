﻿using System.Collections;
using UnityEngine;
using Zenject;

public class RocketGenerator : MonoBehaviour
{
    [SerializeField] private GameObject rocketPrefab;
    private GameObject player;
    private float rocketWidth;

    public bool GeneratorActive = true;

    [Inject]
    private void InjectVariable(PlayerMovement player)
    {
        this.player = player.gameObject;
    }

    private void Start()
    {
        rocketWidth = rocketPrefab.GetComponent<SpriteRenderer>().bounds.extents.x;
        StartCoroutine(GenerateRockets());
    }

    private IEnumerator GenerateRockets()
    {
        while (GeneratorActive)
        {
            GameObject rocket = ObjectPooler.instance.GetPooledObject(rocketPrefab.tag);
            Vector2 screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
            rocket.transform.position = new Vector2(player.transform.position.x, screenBounds.y);
            rocket.SetActive(true);
            StartCoroutine(MoveRocketDown(rocket));
            yield return new WaitForSeconds(200f * Time.deltaTime);
        }
    }

    private IEnumerator MoveRocketDown(GameObject rocket)
    {
        while (rocket.activeInHierarchy)
        {
            rocket.transform.Translate(Vector2.up * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }
}