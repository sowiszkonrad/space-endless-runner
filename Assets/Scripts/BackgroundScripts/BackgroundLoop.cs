﻿using UnityEngine;

public class BackgroundLoop : MonoBehaviour
{
    public GameObject[] levels;
    private Vector2 screenBounds;
    public Transform backgroundsLayerHolder;

    private Vector3 lastScreenPosition;

    private void Start()
    {
        screenBounds = ScreenBounds.GetScreenBounds(Camera.main);
        lastScreenPosition = transform.position;
        foreach (GameObject obj in levels)
        {
            LoadChildObjects(obj);
        }
    }

    private void LoadChildObjects(GameObject obj)
    {
        float objectHeight = obj.GetComponent<SpriteRenderer>().bounds.size.y;
        GameObject layer = Instantiate(obj);
        for (int i = 0; i <= 3; i++)
        {
            GameObject layerInstance = Instantiate(layer);
            layerInstance.transform.SetParent(obj.transform);
            float yPosition = objectHeight >= screenBounds.y ? objectHeight * i : obj.transform.position.y + (objectHeight + screenBounds.y) * i * 2;
            layerInstance.transform.position = new Vector3(obj.transform.position.x, yPosition, obj.transform.position.z);
            layerInstance.name = obj.name + i;
        }
        Destroy(layer);
        Destroy(obj.GetComponent<SpriteRenderer>());
    }

    private void RepositionChildObjects(GameObject obj)
    {
        Transform[] children = obj.GetComponentsInChildren<Transform>();
        if (children.Length > 1)
        {
            GameObject firstChild = children[1].gameObject;
            GameObject lastChild = children[children.Length - 1].gameObject;
            float halfObjectHight = lastChild.GetComponent<SpriteRenderer>().bounds.extents.y;
            if (transform.position.y + screenBounds.y > lastChild.transform.position.y + halfObjectHight)
            {
                firstChild.transform.SetAsLastSibling();
                float yPosition = halfObjectHight >= screenBounds.y / 2 ? halfObjectHight : (halfObjectHight + screenBounds.y);
                firstChild.transform.position = new Vector3(lastChild.transform.position.x, lastChild.transform.position.y + yPosition * 2, lastChild.transform.position.z);
            }
        }
    }

    private void LateUpdate()
    {
        foreach (Transform obj in backgroundsLayerHolder)
        {
            RepositionChildObjects(obj.gameObject);
            float parallaxSpeed = 1 - Mathf.Clamp01(Mathf.Abs(transform.position.z / obj.transform.position.z));
            float difference = transform.position.y - lastScreenPosition.y;
            obj.transform.Translate(Vector2.up * difference * parallaxSpeed, Space.World);
        }
        lastScreenPosition = transform.position;
    }
}