﻿using UnityEngine;
using Zenject;

public class AppInstaller : MonoInstaller
{
    [SerializeField] private GameObject player;

    public override void InstallBindings()
    {
        Container.Bind<PlayerMovement>().FromComponentOn(player).AsSingle();
    }
}